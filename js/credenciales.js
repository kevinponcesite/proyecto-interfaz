function obtenerListaUsuarios(){
    var listaUsuarios = JSON.parse(localStorage.getItem('listaUsuariosLs'));

    if (listaUsuarios == null) {
        listaUsuarios =
        [
            ['Anthony', 'Palacios', 'aPalacios@gmail.com','pala123'],
            ['Axel', 'Arteaga', 'aAxel@gmail.com','arteaga123'],
            ['Kevin', 'Ponce', 'geo@gmail.com','geo123'],
            ['Welinton', 'Guerrero', 'gWelinton@gmail.com','alex123'],
            ['Cristhopher', 'Alcivar', 'calcivar@gmail.com','alcivar123'],
            ['Aldair', 'Lino', 'alino@gmail.com','lino123']
        ]
    }
    return listaUsuarios;
}

function validarCredenciales(pCorreo,pContrasenna){
    var listaUsuarios = obtenerListaUsuarios();
    var bAcceso = false;
    for (var i = 0; i < listaUsuarios.length; i++){
        if (pCorreo == listaUsuarios[i][2] && pContrasenna == listaUsuarios[i][3]) {
            bAcceso = true;
            sessionStorage.setItem('usuarioActivo', listaUsuarios[i][0] + ' ' + listaUsuarios[i][1]);

        }
    }
    return bAcceso;
}