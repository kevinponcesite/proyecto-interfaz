document.querySelector('#btnIngresar').addEventListener('click', 
iniciarSesion);

document.querySelector('#span').addEventListener('click', 
registro);

function iniciarSesion(){
    var sCorreo = '';
    var sContrasenna = '';
    var bAcceso = false;
    
    sCorreo = document.querySelector('#txtCorreo').value;
    sContrasenna = document.querySelector('#txtContrasenna').value;

    bAcceso = validarCredenciales(sCorreo, sContrasenna);
    if (bAcceso == true) {
        ingresar();
    }

    function ingresar(){
        window.location.href = 'inicio.html';  
    }

}

function registro(){
    window.location.href = 'registro.html';
}